package ru.t1.dkozyaikin.tm.api.controller;

public interface ICommandController {

    void displayWelcome();

    void displayHelp();

    void displayVersion(String version);

    void displayAbout();

    void displayInfo();

    void displayArguments();

    void displayCommands();

    void clearOutput();

}
