package ru.t1.dkozyaikin.tm.component;

import ru.t1.dkozyaikin.tm.api.controller.ICommandController;
import ru.t1.dkozyaikin.tm.api.controller.IProjectController;
import ru.t1.dkozyaikin.tm.api.controller.IProjectTaskController;
import ru.t1.dkozyaikin.tm.api.controller.ITaskController;
import ru.t1.dkozyaikin.tm.api.repository.ICommandRepository;
import ru.t1.dkozyaikin.tm.api.repository.IProjectRepository;
import ru.t1.dkozyaikin.tm.api.repository.ITaskRepository;
import ru.t1.dkozyaikin.tm.api.service.ICommandService;
import ru.t1.dkozyaikin.tm.api.service.IProjectService;
import ru.t1.dkozyaikin.tm.api.service.IProjectTaskService;
import ru.t1.dkozyaikin.tm.api.service.ITaskService;
import ru.t1.dkozyaikin.tm.controller.CommandController;
import ru.t1.dkozyaikin.tm.controller.ProjectController;
import ru.t1.dkozyaikin.tm.controller.ProjectTaskController;
import ru.t1.dkozyaikin.tm.controller.TaskController;
import ru.t1.dkozyaikin.tm.enumerated.Status;
import ru.t1.dkozyaikin.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.dkozyaikin.tm.exception.system.CommandNotSupportedException;
import ru.t1.dkozyaikin.tm.model.Project;
import ru.t1.dkozyaikin.tm.model.Task;
import ru.t1.dkozyaikin.tm.repository.CommandRepository;
import ru.t1.dkozyaikin.tm.repository.ProjectRepository;
import ru.t1.dkozyaikin.tm.repository.TaskRepository;
import ru.t1.dkozyaikin.tm.service.CommandService;
import ru.t1.dkozyaikin.tm.service.ProjectService;
import ru.t1.dkozyaikin.tm.service.ProjectTaskService;
import ru.t1.dkozyaikin.tm.service.TaskService;
import ru.t1.dkozyaikin.tm.util.TerminalUtil;

import static ru.t1.dkozyaikin.tm.constant.ArgumentConst.*;
import static ru.t1.dkozyaikin.tm.constant.TerminalConst.*;

public final class Boostrap {

    private static final String VERSION = "1.15.0";
    private final ICommandRepository commandRepository = new CommandRepository();
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final ITaskRepository taskRepository = new TaskRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final ICommandController commandController = new CommandController(commandService);
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final ITaskService taskService = new TaskService(taskRepository);
    private final ITaskController taskController = new TaskController(taskService);
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);
    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private static void exit() {
        System.exit(0);
    }

    public void run(final String[] args) {
        projectRepository.add(new Project("P4", "P1D", Status.IN_PROGRESS));
        projectRepository.add(new Project("P2", "P2D", Status.NOT_STARTED));
        projectRepository.add(new Project("P3", "P3D", Status.COMPLETED));
        projectRepository.add(new Project("P1", "P4D", Status.IN_PROGRESS));
        taskRepository.add(new Task("C TASK", "T1D"));
        taskRepository.add(new Task("A TASK", "T2D"));
        taskRepository.add(new Task("D TASK", "T3D"));
        taskRepository.add(new Task("B TASK", "T4D"));
        commandController.displayWelcome();
        if (processArguments(args)) exit();
        processCommands();
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("\nEnter command:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[DONE]");
            } catch (final Exception e) {
                System.out.println(e.getMessage());
                System.out.println("[FAIL]");
            }
        }
    }

    private void processCommand(final String parameter) throws CommandNotSupportedException {
        if (parameter == null || parameter.isEmpty()) {
            throw new CommandNotSupportedException();
        }
        switch (parameter) {
            case CMD_HELP:
                commandController.displayHelp();
                break;
            case CMD_VERSION:
                commandController.displayVersion(VERSION);
                break;
            case CMD_ABOUT:
                commandController.displayAbout();
                break;
            case CMD_INFO:
                commandController.displayInfo();
                break;
            case CMD_ARGUMENTS:
                commandController.displayArguments();
                break;
            case CMD_COMMANDS:
                commandController.displayCommands();
                break;
            case CMD_CLEAR_OUTPUT:
                commandController.clearOutput();
                break;
            case CMD_PROJECT_CREATE:
                projectController.createProject();
                break;
            case CMD_PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CMD_PROJECT_LIST:
                projectController.displayProjects();
                break;
            case CMD_PROJECT_DISPLAY_BY_ID:
                projectController.displayProjectById();
                break;
            case CMD_PROJECT_DISPLAY_BY_INDEX:
                projectController.displayProjectByIndex();
                break;
            case CMD_PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case CMD_PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case CMD_PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case CMD_PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case CMD_PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case CMD_PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case CMD_PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case CMD_PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case CMD_PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case CMD_PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case CMD_TASK_CREATE:
                taskController.createTask();
                break;
            case CMD_TASK_CLEAR:
                taskController.clearTasks();
                break;
            case CMD_TASK_LIST:
                taskController.displayTasks();
                break;
            case CMD_TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case CMD_TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            case CMD_TASK_DISPLAY_BY_ID:
                taskController.displayTaskById();
                break;
            case CMD_TASK_DISPLAY_BY_PROJECT_ID:
                taskController.displayTaskByProjectId();
                break;
            case CMD_TASK_DISPLAY_BY_INDEX:
                taskController.displayTaskByIndex();
                break;
            case CMD_TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case CMD_TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case CMD_TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case CMD_TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case CMD_TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case CMD_TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case CMD_TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case CMD_TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case CMD_TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case CMD_TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case CMD_EXIT:
                exit();
            default:
                throw new CommandNotSupportedException(parameter);
        }
    }

    private boolean processArguments(String[] args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private void processArgument(final String parameter) throws ArgumentNotSupportedException {
        if (parameter == null || parameter.isEmpty()) {
            throw new ArgumentNotSupportedException();
        }
        switch (parameter) {
            case ARG_HELP:
                commandController.displayHelp();
                break;
            case ARG_VERSION:
                commandController.displayVersion(VERSION);
                break;
            case ARG_ABOUT:
                commandController.displayAbout();
                break;
            case ARG_INFO:
                commandController.displayInfo();
                break;
            case ARG_ARGUMENTS:
                commandController.displayArguments();
                break;
            case ARG_COMMANDS:
                commandController.displayCommands();
                break;
            default:
                throw new ArgumentNotSupportedException(parameter);
        }
    }

}
